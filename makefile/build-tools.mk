#CFLAGS        = 
#CXXFLAGS      = 
#LDFLAGS       = 
#ASFLAGS       = 
#INCPATH       = 

ifeq ($(OS), Windows_NT)
    HOST_PLATFORM = windows
else
    HOST_PLATFORM = linux
endif

ifdef SRCTYPES
    -include $(foreach st,$(SRCTYPES),$(BUILDTOOLSDIR)/makefile/sources/$(st).mk)
endif

ifdef LIBCOLLECTION
    -include $(foreach lc,$(LIBCOLLECTION),$(BUILDTOOLSDIR)/makefile/libs/$(lc).mk)
endif

ifdef PKG-CONFIG
    include $(BUILDTOOLSDIR)/makefile/libs/pkg-config.mk
endif

ifdef PKG-CONFIG-EXTRA
    include $(BUILDTOOLSDIR)/makefile/libs/pkg-config-extra.mk
endif

include $(BUILDTOOLSDIR)/makefile/hosts/$(HOST_PLATFORM).mk

ifdef DEVICE
    include $(BUILDTOOLSDIR)/makefile/devices/$(DEVICE).mk
endif

ifndef TARGET_PLATFORM
    TARGET_PLATFORM = $(HOST_PLATFORM)
endif

ifneq ($(TARGET_PLATFORM), $(HOST_PLATFORM))
    include $(BUILDTOOLSDIR)/makefile/toolchains/$(TARGET_ARCH)-$(TARGET_PLATFORM)-$(TARGET_ABI)-$(COMPILER).mk
endif

include $(BUILDTOOLSDIR)/makefile/compilers/$(COMPILER).mk

ifndef BUILDDIR
    BUILDDIR = build
endif

include $(BUILDTOOLSDIR)/makefile/rules/default.mk

ifdef FLASHPROG
    include $(BUILDTOOLSDIR)/makefile/rules/flash.mk
endif
