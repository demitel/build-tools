CP              = cp -f
RM              = rm -f
MV              = mv -f
MKDIR           = mkdir -p
RMDIR           = rmdir

TAR             = tar -cf
COMPRESS        = gzip -9f
SED             = sed
COPY_FILE       = $(CP)
COPY_DIR        = $(CP) -r
INSTALL_FILE    = install -m 644 -p
INSTALL_DIR     = $(COPY_DIR)
INSTALL_PROGRAM = install -m 755 -p
SYMLINK         = ln -f -s
CHK_DIR_EXISTS  = test -d
DEVNULL         = /dev/null
TMPDIR          = /tmp
