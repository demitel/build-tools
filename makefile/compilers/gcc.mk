CC            = $(TOOLCHAIN_PREFIX)gcc
CXX           = $(TOOLCHAIN_PREFIX)g++
LINK          = $(TOOLCHAIN_PREFIX)g++
AS            = $(TOOLCHAIN_PREFIX)as
STRIP         = $(TOOLCHAIN_PREFIX)strip
AR            = $(TOOLCHAIN_PREFIX)ar crs
OBJCOPY       = $(TOOLCHAIN_PREFIX)objcopy
OBJDUMP       = $(TOOLCHAIN_PREFIX)objdump

CFLAGS += -MD -MP
CXXFLAGS += -MD -MP
ifneq ($(filter $(OPTIONS), DEBUG), )
    CFLAGS += -Wa,-adhlns="$(addsuffix .lst, $(basename $@))"
    CFLAGS += -g3 -gdwarf-2
    CXXFLAGS += -Wa,-adhlns="$(addsuffix .lst, $(basename $@))"
    CXXFLAGS += -g3 -gdwarf-2
endif

ifneq ($(filter $(OPTIONS), MAP), )
    LDFLAGS += -Wl,-Map,"$(TARGET).map"
endif
