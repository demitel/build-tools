TOOLCHAIN_PREFIX = $(TARGET_ARCH)-$(TARGET_PLATFORM)-$(TARGET_ABI)-

# Use individual sections for every function and data
CFLAGS += -ffunction-sections -fdata-sections
CXXFLAGS += -ffunction-sections -fdata-sections
# Link only used sections
LDFLAGS += -Wl,-gc-sections

# No start files needed for bare metsll (-none-) systems
LDFLAGS += -nostartfiles
