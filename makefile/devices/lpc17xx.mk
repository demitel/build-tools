TARGET_ARCH = arm
MCPU = cortex-m3
DEFINES += -D__LPC17XX__
CFLAGS += -mcpu=$(MCPU) -mthumb
CXXFLAGS += -mcpu=$(MCPU) -mthumb
LDFLAGS += -mcpu=$(MCPU) -mthumb

DEVICESERIES = lpc17xx

ifndef TARGET_PLATFORM
    TARGET_PLATFORM = none
endif

ifndef TARGET_ABI
    TARGET_ABI = eabi
endif

FO := $(FLASH_ORIGIN)
FL := $(FLASH_LENGTH)
SO := $(SRAM_ORIGIN)
SL := $(SRAM_LENGTH)

ifdef FLASH_UNUSED_BEGIN
    FO := $(FO)+$(FLASH_UNUSED_BEGIN)
    FL := $(FL)-$(FLASH_UNUSED_BEGIN)
endif
ifdef SRAM_UNUSED_BEGIN
    SO := $(SO)+$(SRAM_UNUSED_BEGIN)
    SL := $(SL)-$(SRAM_UNUSED_BEGIN)
endif
ifdef FLASH_USE_ONLY
    FL := $(FLASH_USE_ONLY)
endif
ifdef SRAM_USE_ONLY
    SL := $(SRAM_USE_ONLY)
endif

$(shell $(SED) ' \
    s/\$$(FLASH_ORIGIN)/$(FO)/; \
    s/\$$(FLASH_LENGTH)/$(FL)/; \
    s/\$$(SRAM_ORIGIN)/$(SO)/; \
    s/\$$(SRAM_LENGTH)/$(SL)/; \
    ' $(BUILDTOOLSDIR)/arm/linker-scripts/lpc17xx.ld > $(TMPDIR)/$(DEVICE).ld)

LDFLAGS += -T"$(TMPDIR)/$(DEVICE).ld"
