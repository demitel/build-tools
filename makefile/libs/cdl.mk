CDLINC = $(BUILDTOOLSDIR)/arm/inc/CDL/
CDLLIB = $(BUILDTOOLSDIR)/arm/lib/

ifeq ($(filter $(CDLINC), $(INCPATH)), )
    INCPATH += $(CDLINC)
endif

ifeq ($(filter $(CDLLIB), $(LIBPATH)), )
    LIBPATH += $(CDLLIB)
endif

LIBS += cdl_lpc17xx
