CMSISINC =  $(BUILDTOOLSDIR)/arm/inc/CMSIS/
CMSISLIB = $(BUILDTOOLSDIR)/arm/lib/

ifeq ($(filter $(CMSISINC), $(INCPATH)), )
    INCPATH += $(CMSISINC)
endif

ifeq ($(filter $(CMSISLIB), $(LIBPATH)), )
    LIBPATH += $(CMSISLIB)
endif

LIBS += cmsis_lpc17xx
