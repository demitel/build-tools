include $(BUILDTOOLSDIR)/makefile/rules/objects.mk

OBJECTS = $(patsubst %.o,$(OBJDIR)/%.o,$(addsuffix .o, $(basename $(SOURCES))))
LISTS += $(patsubst %.o,$(OBJDIR)/%.o,$(addsuffix .lst, $(basename $(SOURCES))))
LDFLAGS += $(addprefix -L,$(LIBPATH)) $(addprefix -l,$(LIBS))

bin: $(BUILDDIR)/$(TARGET).bin

hex: $(BUILDDIR)/$(TARGET).hex

lst: $(BUILDDIR)/$(TARGET).lst

lib: $(BUILDDIR)/lib$(TARGET).a

so: $(BUILDDIR)/lib$(TARGET).so

elf: $(BUILDDIR)/$(TARGET).elf
ifneq ($(filter $(OPTIONS), DEBUG), )
	@echo " STRIP $@"
	@$(STRIP) "$@"
endif

clean:
	@echo " CLEAN"
#	@$(RM) $(OBJECTS) $(TARGET).elf  $(TARGET).bin $(TARGET).lst lib$(TARGET).a $(TARGET).map $(TARGET).hex $(LISTS) $(DEPS)
	@$(RM) -r $(BUILDDIR)

$(OBJECTS): $(SOURCES)

-include $(DEPS)

$(BUILDDIR)/%.elf: $(OBJECTS)
	@echo " LD $@"
	@$(LINK) -o "$@" $^ $(LDFLAGS)

$(BUILDDIR)/%.bin: $(BUILDDIR)/%.elf
	@echo " OBJCOPY $@"
	@$(OBJCOPY) -O binary "$<" "$@"
	@$(BUILDTOOLSDIR)/arm/bin/csumm "$@" > $(DEVNULL)

$(BUILDDIR)/%.hex: $(BUILDDIR)/%.elf
	@echo " OBJCOPY $@"
	@$(OBJCOPY) -O hex "$<" "$@"

$(BUILDDIR)/%.lst: $(BUILDDIR)/%.elf
	@echo " OBJDUMP $@"
	@$(OBJDUMP) "$<" --source --all-headers --demangle --line-numbers --wide > "$@"

$(BUILDDIR)/%.a: $(OBJECTS)
	@echo " MKLIB $@"
	@$(AR) "$@" $^

$(BUILDDIR)/%.so: $(OBJECTS)
	@echo " MKLIB DYN $@"
	@$(LINK) -o "$@" $^ -shared $(LDFLAGS)

.PHONY: elf bin hex lst lib clean
