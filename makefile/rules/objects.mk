CFLAGS += $(OPT)
CFLAGS += $(addprefix -I,$(INCPATH)) $(addprefix -I,$(SRCPATH))
CXXFLAGS += $(OPT)
CXXFLAGS += $(addprefix -I,$(INCPATH)) $(addprefix -I,$(SRCPATH))
OBJDIR = $(BUILDDIR)/obj

$(OBJDIR)/%.o: %.c
	@echo " CC $@"
	@$(MKDIR) $(dir $@)
	@$(CC) $(CFLAGS) -c "$<" -o "$@"

$(OBJDIR)/%.o: %.cpp
	@echo " CPP $@"
	@$(MKDIR) $(dir $@)
	@$(CXX) $(CXXFLAGS) -c "$<" -o "$@"

$(OBJDIR)/%.o: %.s
	@echo " AS $@"
	@$(MKDIR) $(dir $@)
	@$(AS) $(ASFLAGS) "$<" -o "$@"

$(OBJDIR)/%.o: %.S
	@echo " AS $@"
	@$(MKDIR) $(dir $@)
	@$(AS) $(ASFLAGS) "$<" -o "$@"
