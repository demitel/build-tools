flash: flash_$(FLASHPROG)

flash_openocd: $(BUILDDIR)/$(TARGET).bin
	@echo " FLASH $< BEGIN"
	@echo "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
	@$(SED) 's!\$$(TARGET)!$(BUILDDIR)/$(TARGET).bin!' $(BUILDTOOLSDIR)/openocd/flash.cfg > $(TMPDIR)/flash.cfg
	openocd -f interface/$(FLASHDEV).cfg -f target/$(DEVICESERIES).cfg -c "adapter_khz 650"  -f $(TMPDIR)/flash.cfg
	@$(RM) $(TMPDIR)/flash.cfg
	@echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
	@echo " FLASH $< END"

flash_lpc21isp: $(TARGET).bin
	@echo " FLASH $< BEGIN"
	@echo "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
	@lpc21isp -bin $(TARGET).bin $(FLASHDEV) 19200 12000
	@echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
	@echo " FLASH $< END"

.PHONY: flash flash_lpc21isp flash_openocd
