/* chksum.c: compute checksum for LPC2xxx ISP */
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int fd;
    unsigned int *binary;
    unsigned int sum=0, i;
    if (argc!=2)
    {
	fprintf(stderr, "Usage: chksum <filename>\n");
	exit(2);
    }

    fd = open(argv[1], O_RDWR);
    if (fd == -1)
    {
	perror("Can't open file\n");
	exit(2);
    }
    binary = mmap(0, 0xff, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (binary==MAP_FAILED)
    {
	perror("Can't mmap\n");
	exit(2);
    }
    for(i=0; i<8; i++) sum += binary[i];
    printf("Before patching: sum = %X\n", sum);
    sum = 0;
    for(i=0; i<5; i++) sum += binary[i];
    sum+=binary[6];
    sum+=binary[7];
    binary[5] = -sum;
    sum = 0;
    for(i=0; i<8; i++) sum += binary[i];
    printf("After patching: sum = %X\n", sum);
    munmap((void*) binary, 0xff);
    return 0;
}
